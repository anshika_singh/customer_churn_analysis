In this project I have built a machine learning model to predict customer churn as it is one the most important factor that companies need to monitor
,especially those that depend on subscription based revenue streams. In this model I have used Keras package to build Artificial Neural Network
In addition to this package, other packages that i have used are as follows :
lime,
corr,
rsample,
yardstick,
carret and
recipes
The dataset used includes information about customers who left within last month, services that each customer signed up for, customer account
information and demographic information about the customers.